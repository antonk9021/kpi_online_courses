
from flask import Flask, render_template, flash, session, request, redirect, url_for, g
from flask_login import LoginManager, login_user, logout_user, current_user, login_required
from flask_mail import Mail, Message
from datetime import datetime
from flask_cors import CORS
import base64
import controllers as ctrl
from forms import *
from definitions import *
from watchdogs import *


class Config(object):
    """Configuration for Flask app"""
    SECRET_KEY = os.urandom(64)
    CSRF_ENABLED = True
    JSON_AS_ASCII = False
    MAIL_SERVER = os.environ.get("MAIL_SERVER")
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    MAIL_USERNAME = os.environ.get("MAIL_USERNAME")
    MAIL_PASSWORD = os.environ.get("MAIL_PASSWORD")
    ADMINS = [f"{MAIL_USERNAME}@gmail.com"]

app = Flask(__name__)
app.config.from_object(Config)
CORS(app)
mail = Mail(app)
lm = LoginManager()
lm.init_app(app)
lm.login_message = None
lm.login_view = "login"

# oldCode = os.urandom(32)
# print(base64.b32encode(oldCode).decode())
# newCode = base64.b32encode(oldCode).decode()
# print(base64.b32decode(newCode) == oldCode)


@lm.user_loader
def load_user(email):
    if session["user_type"] == "student":  # FIXME: 'user_type' does not work properly with remember_me
        return ctrl.Student(email)
    if session["user_type"] == "tutor":
        return ctrl.Tutor(email)


@app.route('/', methods=["GET", "POST"])
@app.route("/home", methods=["GET", "POST"])
@login_required
def home():
    if current_user is None or not current_user.is_authenticated or not session["email_verified"]:
        return redirect(url_for("login"))
    user = current_user
    if session["user_type"] == "student":
        title = "student"
    elif session["user_type"] == "tutor":
        title = "tutor"
    elif session["user_type"] == "admin":
        title = "admin"
    else:
        raise AttributeError("No title set for this session")
    return render_template("home.html", title=title, user=user, style="home")


@app.route("/login", methods=["GET", "POST"])
def login():
    if current_user is not None and current_user.is_authenticated:
        return redirect(url_for("home"))
    form = LoginForm()
    if request.method == "GET":
        return render_template("login.html", form=form, style="start")
    if form.validate_on_submit():
        if request.form.get("is_student") == 'y':
            user = ctrl.Student(request.form.get("email"))
            session["user_type"] = "student"
        else:
            user = ctrl.Tutor(request.form.get("email"))
            session["user_type"] = "tutor"
        if user.info is None or not user.verify_passwd(request.form.get("passwd")):
            flash("Error")
            return render_template("login.html", form=form, style="start")
        login_user(user, remember=form.remember_me)
        session["email_verified"] = True

        return redirect(url_for("home"))


@app.route("/email_verification", methods=["GET", "POST"])
@login_required
def email_verification():
    if not session.get("email_verification_code"):
        session["email_verification_code"] = os.urandom(32)
    form = EmailVerificationForm()
    if request.method == "GET":
        msg = Message("Email verification for Kovalov IASA project", sender=app.config["ADMINS"][0], recipients=[current_user["email"]])
        msg.html = f"Here is your verification code: <br><br> " \
                   f"{base64.b32encode(session['email_verification_code']).decode('utf-8')}"
        with app.app_context():
            mail.send(msg)
        return render_template("email_verification.html", email=current_user["email"], form=form, style="start")
    if form.validate_on_submit():
        code_entered = base64.b32decode(request.form.get("code_entered"))
        if code_entered != session["email_verification_code"]:
            flash("Wrong verification code")
            return render_template("email_verification.html", form=form, style="start")
        session["email_verified"] = True
        session["email_verification_code"] = None
        return redirect(url_for("home"))


@app.route("/signup", methods=["GET", "POST"])
def signup():
    if current_user is not None and current_user.is_authenticated:
        return redirect(url_for("home"))
    form = RegistrationForm()
    if request.method == "GET":
        return render_template("signup.html", form=form, style="start",
                               min_years=MIN_YEARS.strftime("%Y-%m-%d"),
                               max_years=MAX_YEARS.strftime("%Y-%m-%d"))
    if form.validate_on_submit():
        name = request.form.get("fname") + ' ' + request.form.get("lname")
        email = request.form.get("email")
        phone = request.form.get("phone")
        date_of_birth = request.form.get("date_of_birth")
        passwd = request.form.get("passwd")
        repeat_passwd = request.form.get("repeat_passwd")
        if passwd != repeat_passwd:
            flash("passwd_error")
        user_type = request.form.get("user_type")

        try:
            if user_type == "student":
                user = ctrl.Student.register(name, email, phone, 0,
                                             date_of_birth,
                                             datetime.utcnow().strftime("%Y-%m-%d"), passwd)
            else:
                user = ctrl.Tutor.register(name, email, phone, 0, passwd)
        except SqlInjectionAttempt:
            msg = Message("SQL INJECTION ATTEMPT", sender=app.config["ADMINS"][0],
                          recipients=app.config["ADMINS"])
            msg.html = f"SQL injection attempt, kek<br><br> "  # TODO: remove stupid `kek`
            with app.app_context():
                mail.send(msg)
            return redirect(url_for("error"))
        except UserAlreadyExists:
            flash("Email already exists")
            return render_template("signup.html", form=form, style="start")

        user.is_authenticated = True
        session["user_type"] = user_type
        session["email_verified"] = False

        login_user(user, remember=form.remember_me)
        return redirect(url_for("email_verification"))


# @app.before_request
# def before_request():
#     g.user = current_user

@app.route("/error")
def error():
    return render_template("error.html")


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("login"))


if __name__ == '__main__':
    app.run("0.0.0.0", port=5000)
