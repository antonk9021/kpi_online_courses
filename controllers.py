from abc import abstractmethod
import re
import hashlib
import sqlalchemy
from datetime import datetime
from definitions import *
from watchdogs import *


class User(object):
    def __init__(self, email, new_user=False, uri=MYSQL_URI):
        self._engine = sqlalchemy.create_engine(uri, echo=False)
        if not new_user:
            self.info = email
            self.is_authenticated = True
            if self.info is not None:
                self.passwd = email

    def __str__(self):
        return ', '.join([str(item) for item in self.info.values()]) \
            if self.info is not None \
            else "No results"

    def __iter__(self):
        for field in self.info:
            yield field

    def __getitem__(self, item):
        return self.info[item] if self.info is not None else None

    @property
    def db_columns(self):
        return ', '.join(self._db_columns)

    @db_columns.setter
    def db_columns(self, columns: list):
        self._db_columns = columns

    @property
    def info(self):
        return self._info

    @info.setter
    def info(self, email):
        query = f"SELECT {self.db_columns} FROM {self.table} WHERE email = \'{email}\';"
        result = self._execute_sql(query).first()
        self._info = dict(result) if result is not None else None

    @property
    def table(self):
        return self._table

    @table.setter
    def table(self, table_str):
        self._table = table_str


    @property
    def passwd(self):
        raise AttributeError("Permission denied: passwd is not a readable attribute")

    @passwd.setter
    def passwd(self, email):
        query = f"SELECT passwd FROM {self.table} WHERE email = \'{email}\';"
        self._passwd_hash = self._execute_sql(query).first()[0]

    def verify_passwd(self, passwd, salt=SALT):
        to_be_checked = passwd + salt
        h = hashlib.md5(to_be_checked.encode())
        return h.hexdigest() == self._passwd_hash

    # def email_exists(self, email):
    #     query = f"SELECT name FROM {self.table} WHERE email = \'{email}\';"
    #     result = self._execute_sql(query).first()
    #     return result is not None

    @property
    def is_authenticated(self):
        return self._is_authenticated

    @is_authenticated.setter
    def is_authenticated(self, state: bool):
        self._is_authenticated = state

    @staticmethod
    def is_active():
        return True

    @staticmethod
    def is_anonymous():
        return False

    def get_id(self):
        """Returns user`s email which would serve as an id for Flask session"""
        return self.info["email"] if self.info is not None else None

    def _execute_sql(self, query):
        with self._engine.connect() as c:
            result = c.execute(query)
        return result

    @classmethod
    def register(cls, *values, salt=SALT):
        email = values[1]
        passwd = values[-1] + salt
        h = hashlib.md5(passwd.encode()).hexdigest()
        values = values[:-1] + (h, )
        new_user = cls(email, new_user=True)
        sql_injection_attempt = filter(lambda word: any(prohibited_word
                                                        in str(word) for prohibited_word
                                                        in ["DROP", "DROP TABLE", "SELECT", "FROM"]), values)
        for alert in sql_injection_attempt:
            if alert is not None:
                raise SqlInjectionAttempt
        query = f"INSERT INTO {new_user.table} " \
                f"({new_user.db_columns.replace('targetCourseId', '').replace('courseId, groupId', '') + 'passwd'}) " \
                f"VALUES {values};"
        try:
            new_user._execute_sql(query)
        except sqlalchemy.exc.IntegrityError:
            raise UserAlreadyExists
        new_user.info = email
        new_user.passwd = email
        return new_user


class Student(User):
    def __init__(self, email, new_user=False):
        self.table = "Students"
        self.db_columns = ["name", "email", "phoneNo", "balance",
                           "birthDate", "registrationDate", "targetCourseId"]
        super().__init__(email, new_user)


class Tutor(User):
    def __init__(self, email, new_user=False):
        self.table = "Tutors"
        self.db_columns = ["name", "email", "phoneNo",
                           "hoursPerWeek", "courseId", "groupId"]
        super().__init__(email, new_user)


# class StudyGroup(Entity):
#     def __init__(self):
#         super().__init__()
#         pass
