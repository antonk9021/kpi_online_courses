import os
import json
from datetime import datetime, timedelta




with open("db_settings.json", 'r') as f:
    j = json.loads(f.read())

USER = j["user"]
PASSWORD = j["password"]
HOST = j["host"]
PORT = j["port"]
DB_NAME = j["db_name"]
MYSQL_URI = f"mysql://{USER}:{PASSWORD}@{HOST}:{PORT}/{DB_NAME}"
SALT = "kek"
MIN_YEARS = datetime.today() - timedelta(days=6570)  # Subtracts 18 years from the current date
MAX_YEARS = datetime.today() - timedelta(days=36500)  # Subtracts 100 years from the current date





