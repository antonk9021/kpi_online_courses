from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from wtforms import StringField, PasswordField, BooleanField, RadioField
from wtforms.fields.html5 import DateField
from wtforms.validators import Required


class LoginForm(FlaskForm):
    email = StringField("email", validators=[Required()])
    passwd = PasswordField("password", validators=[Required()])
    remember_me = BooleanField("remember_me", default=False)
    is_student = BooleanField("is_student", default=True)


class RegistrationForm(FlaskForm):
    fname = StringField("fname", validators=[Required()])
    lname = StringField("lname", validators=[Required()])
    email = StringField("email", validators=[Required()])
    phone = StringField("phone", validators=[Required()])
    date_of_birth = DateField("date_of_birth", format="%Y-%m-%d")
    passwd = PasswordField("password", validators=[Required()])
    repeat_passwd = PasswordField("repeat_password", validators=[Required()])
    remember_me = BooleanField("remember_me", default=False)
    user_type = RadioField(choices=[("tutor", "I'm a tutor"), ("student", "I'm a student")],
                           validators=[Required()])


class EmailVerificationForm(FlaskForm):
    code_entered = StringField("code_entered", validators=[Required()])
