window.onload = function () {


    var emailField = document.getElementById("email");
    var es = emailField.style;
    es.background = "#ffb3b3"; // rgb 255,179,179
    es.color = "red";

    var startColor = [255,179,179];
    var endColor = [242,242,242];
    var currentColor = startColor;

    var steps = 10;
    var stepCount = 0;

    var redChange = (startColor[0] - endColor[0]) / steps;
    var greenChange = (startColor[1] - endColor[1]) / steps;
    var blueChange = (startColor[2] - endColor[2]) / steps;

    var fadeTimer = setInterval(function(){
        currentColor[0] = parseInt(currentColor[0] - redChange);
        currentColor[1] = parseInt(currentColor[1] - greenChange);
        currentColor[2] = parseInt(currentColor[2] - blueChange);

        es.background = 'rgb(' + currentColor.toString() + ')';
        stepCount += 1;

        if (stepCount >= steps){
            es.background = 'rgb(' + endColor.toString() + ')';
            clearInterval(fadeTimer);
        }
    }, 50);

    es.color = "black";

    var warning = document.getElementById("warning");
        warning.innerHTML = "This e-mail has already been registered";
}