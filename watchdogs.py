
class SqlInjectionAttempt(Exception):
    """SQL injection warning watchdog"""
    pass


class UserAlreadyExists(Exception):
    """Watchdog that barks if one tries to register an email twice"""
    pass
